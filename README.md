# Portland
## Discord bot for playing back the radio stream.

### Install and run (Debian)
- `sudo apt install nodejs npm ffmpeg git`
- `git clone https://gitlab.com/aalis/portland`
- `cd portland`
- `npm install discord.js @discordjs/opus express pug ws zlib-sync erlpack libsodium-wrappers utf-8-validate ffmpeg-static`
- `node .`
- Go to http://127.0.0.1:8000/
- Fill the configuration with a `broadcast_url` and `discord_token`. The rest is optional.
- Restart the program

### Usage
#### Default commands
- `%join` - Joins the channel you're in.
- `%leave` - Leaves the currently occupied channel.

#### Configuration
Bots comes with local HTTP server which should NOT be available publicly. Web authentication might be added in future updates.
- `http_port` - Pretty self-explainatory. Specifies on which port HTTP server should listen on. Change applies on restart.
- `discord_token` - Set it to your's Discord application token for bot to run. Change applies on restart.
- `broadcast_name` - Name of the radio stream.
- `broadcast_url` - URL to the station's audio stream. Pause and resume to stream after changing to apply.
- `broadcast_info` - URL to the Icecast's icestats file.
- `admins_only` - If the commands should be executed only by administrators.
- `allowed_users` - Comma-separated list of Discord User ID's that bypass restrictions, if present.
- `prefix` - Prefix of commands, % is default.

