window.addEventListener('load', () => {

	text1.animate([
		{ transform: 'translateX(-100vw) rotate(-45deg)' },
		{ transform: 'translateX(0) rotate(0deg)' }
	], {duration: 350, iteration: 1, fill: 'forwards', easing: 'ease-in-out'});

	text2.animate([
		{ transform: 'translateX(100vw) rotate(45deg)' },
		{ transform: 'translateX(0) rotate(0deg)'}
	], {delay: 350, duration: 350, iteration: 1, fill: 'forwards', easing: 'ease-in-out'});

	dot.animate([
		{ width: '0px' },
		{ width: '5px' },
	], {delay: 700, duration: 150, iteration: 1, fill: 'forwards'});

	dot.animate([
		{ borderRadius: '0px' },
		{ borderRadius: '50%' },
	], {delay: 850, duration: 100, iteration: 1, fill: 'forwards'});

	load.animate([
		{ opacity: 1 },
		{ opacity: 0 }
	], {delay: 1400, duration: 160, iteration: 1, fill: 'forwards'});

	setTimeout(() => load.style.display = 'none', 1560);

	let socket = new WebSocket(`ws://${location.hostname}:${location.port}`);
	socket.onopen = e => console.info('WebSocket active.');
	socket.onerror = () => connlost.style.display = 'block';
	socket.onclose = () => connlost.style.display = 'block';

	socket.onmessage = e => {

		subscribers.innerHTML = '';
		let json = JSON.parse(e.data);

		for ( let i = 0; i < json.length; i++ ) {
			let data = json[i];
			subscribers.innerHTML += `
				<li>
				<img src="${data.icon}" alt="#" />
				<b>${data.channel}</b> in <b>${data.guild}</b> <i>(${data.listeners - 1} listeners)</i>
				<button class="leave_ch" channel="${data.channelid}" title="Leave channel">x</button>
				</li>`;
		}


		let leave_ch = document.getElementsByClassName('leave_ch');
		for ( let i = 0; i < leave_ch.length; i++ ) {
			leave_ch[i].addEventListener('click', function() {
				socket.send(`STREAM:LEAVE:${this.getAttribute('channel')}`);
			}, false);
		}

	};


	cfg_submit.addEventListener('click', (event) => {

		event.preventDefault();

		let input_elements = document.getElementsByTagName('input');
		let to_commit = {};

		for ( let i = 0; i < input_elements.length; i++ )
			if ( input_elements[i].type == 'text' )
				to_commit[input_elements[i].id] = input_elements[i].value;

		console.log(`Sending\n"JSON:${JSON.stringify(to_commit)}"`);
		socket.send(`JSON:${JSON.stringify(to_commit)}`);

		configuration_changed.style.display = 'block';

		configuration_changed.animate([
			{ transform: 'scale(.8) translate(80%, -250%)  rotate(-60deg)', opacity: 0  },
			{ transform: 'scale(.9) translate(80%, -250%)  rotate(-40deg)', opacity: .5 },
			{ transform: 'scale(1)  translate(80%, -250%)  rotate(-20deg)', opacity: 1  },
			{ transform: 'scale(1.1) translate(80%, -250%) rotate(0deg)',   opacity: 1  },
			{ transform: 'scale(1.2) translate(80%, -250%) rotate(20deg)',  opacity: 0  }
		], {duration: 1600, iteration: 1});

		setTimeout(() => configuration_changed.style.display = 'none', 1600);

	}, false);


	pause_stream.addEventListener('click', function () {

		let stream_status = (this.innerHTML.startsWith('Pause') ? 'up' : 'down');

		switch (stream_status) {

			case 'up':
				socket.send('STREAM:PAUSE');
				this.innerHTML = 'Resume stream';
				break;

			case 'down':
				socket.send('STREAM:RESUME');
				this.innerHTML = 'Pause stream';
				break;

		}

	}, false);

	purge_inactive.addEventListener('click', () => socket.send('STREAM:PURGE'), false);

}, false);
