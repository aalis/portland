const request = require('request');

module.exports = {

	getSong: (broadcast_info, func, sourceNo = 0) => {
		request(broadcast_info, {json: true}, (err, res, body) => {
			
			if (err) {
				console.error(err);
				return null;
			}

			let source = body.icestats.source[sourceNo] || body.icestats.source;
			func(source.title);
			
		});
	}

};
