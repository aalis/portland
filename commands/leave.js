module.exports = {
	name: 'leave',
	description: 'Exits the active channel.',
	execute(message, args, broadcast) {

		if (message.guild.voice.channel) {
			message.guild.voice.channel.leave();
			message.reply(', ok! Leaving.');
		} else
			message.reply(', not connected.');

	}
};
