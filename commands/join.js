module.exports = {
	name: 'join',
	description: 'Joins voice channel where the requesting user is present.',
	execute(message, args, broadcast) {

		if (!message.member.voice.channel)
			message.reply(`${message.author}, you're not in channel.`);

		message.member.voice.channel.join().then(
			connection => connection.play(broadcast, {volume: .5}));

		message.reply(`joining **${message.member.voice.channel.name}**...`)

	}
};
