"use strict";

const Discord = require('discord.js');
const Express = require('express');
const WebSock = require('ws');
const fs      = require('fs');
const crypto  = require('crypto');

const icecast = require('./icecast.js');
var icestatus = null;

const bot = new Discord.Client();
bot.commands = new Discord.Collection();
const app = Express();
const wsServer = new WebSock.Server({ noServer: true });

app.set('views', './templates');
app.set('view engine', 'pug');
app.use('/static', Express.static('static'));

var cfg = JSON.parse(fs.readFileSync('cfg.json'));

const broadcast = bot.voice.createBroadcast();
var broadcast_dispatcher = broadcast.play(cfg.broadcast_url);

// >>>> Discord Client
// >>>>>> ready event
bot.on('ready', () => {
	console.info(`Logged into Discord as ${bot.user.tag}`);

	bot.setInterval(() => {
		let song = icecast.getSong(cfg.broadcast_info, (song) => {

			if ( song !== icestatus ) {
				icestatus = song;
				bot.user.setPresence({
					activity: {
						name: `${icestatus} @ ${cfg.broadcast_name}`
					}
				});
			}

		}, parseInt(cfg.bc_source));
	}, parseInt(cfg.broadcast_delay));

});

// >>>>>> command handler
const command_files = fs.readdirSync('commands/').filter(
	file => file.endsWith('.js'));

for (const file of command_files) {
	const command = require(`./commands/${file}`);
	bot.commands.set(command.name, command);

}

// >>>>>> message event
bot.on('message', message => {
	
	// >>>>>>>> if client is only mentioned, return informations about itself
	if (message.mentions.has(bot.user)) {
		message.reply('Hi! I\'m Portland, a bot by the Las Pegasus Radio (<https://www.laspegas.us>).' +
			'\nI\'m using Discord.js to communicate and Express + WebSocket to be configured and controlled.' +
			'\nCheck out my source code at https://gitlab.com/aalis/portland');
		return;
	}

	if (message.content.charAt(0) !== cfg.prefix || !message.guild || message.author.bot)
		return;

	// >>>>>>>> check if: user is a member on a server and
	//          (user is a server admin or is on a list of allowed users)
	if (message.member !== undefined) {
		if (!(message.member.permissions.has('ADMINISTRATOR') && cfg.admins_only) &&
			!cfg.allowed_users.split(',').includes(message.author.id.toString())) {
			message.reply('Error, no permissions.');
			return;
		}
	} else {
		message.reply('Error. I\'m useful only on servers.');
		return;
	}

	let msg_split = message.content.slice(1).split(' ');
	var command = {
		name: msg_split[0],
		args: msg_split.slice(1) };

	if (bot.commands.has(command.name)) {

		try {
			bot.commands.get(command.name).execute(message, command.args, broadcast);
		} catch (err) {
			message.reply('Error. Please try again.');
			console.error(err);
		}

	}

});


// >>>> Express server
app.get('/', (req, res) => res.render('index', {
	cfg: cfg, broadcast: broadcast_dispatcher }));

const httpServer = app.listen(cfg.http_port,
	() => console.info(`HTTP server started on ${cfg.http_port}`));

wsServer.on('connection', clientSock => {
	clientSock.send_interval = setInterval(() => {

		let informations = [];

		for (let i = 0; i < broadcast_dispatcher.broadcast.subscribers.length; i++) {

			let instance = broadcast_dispatcher.broadcast.subscribers[i];
			informations.push({
				'channel': instance.player.voiceConnection.channel.name,
				'channelid': instance.player.voiceConnection.channel.id.toString(),
				'guild': instance.player.voiceConnection.channel.guild.name,
				'icon': instance.player.voiceConnection.channel.guild.iconURL({size: 32, format: 'jpg'}),
				'listeners': instance.player.voiceConnection.channel.members.size
			});

		}

		let info_str = JSON.stringify(informations);
		let hashed_info = crypto.createHash('sha1').update(info_str).digest('base64');

		if ( hashed_info !== clientSock.old_info ) {

			try {
				console.log(`Sending informations to ${clientSock._socket.remoteAddress}...`);
				clientSock.send(info_str);
			} catch (err) {
				console.log(`Connection with ${clientSock._socket.remoteAddress} lost.`);
				clearInterval(clientSock.send_interval);
			}
			clientSock.old_info = hashed_info;
		}

	}, 2000);

	let onCloseEvent = (event) => {
		console.log(`Connection with ${clientSock._socket.remoteAddress} lost.`);
		clearInterval(clientSock.send_interval);
	};

	clientSock.on('close', onCloseEvent);
	clientSock.on('error', onCloseEvent);

	clientSock.on('message', message => {

		console.log(`Received message from ${clientSock._socket.remoteAddress}:\n${message}`);

		if (message.startsWith('JSON')) {
			message = message.slice(5); // remove "JSON:" from the incoming string
			try {
				message = JSON.parse(message)
				cfg = message;
				fs.writeFileSync('cfg.json', JSON.stringify(cfg));
				console.log(`Configuration updated by ${clientSock._socket.remoteAddress}`);
			} catch(error) {
				console.log('JSON parse message error');
				console.dir(error);
			}
		}
		else if (message.startsWith('STREAM')) {
			message = message.split(':');
			switch (message[1]) {

				case 'PAUSE':
					broadcast_dispatcher.pause(true);
					break;

				case 'RESUME':
					broadcast_dispatcher = broadcast.play(cfg.broadcast_url);					
					break;

				case 'PURGE':
					for (let i = 0; i < broadcast.subscribers.length; i++) {
						let channel = broadcast.subscribers[i].player.voiceConnection.channel;
						if (channel.members.size <= 1)
							channel.leave();
					}
					break;

				case 'LEAVE':
					for (let i = 0; i < broadcast.subscribers.length; i++) {
						let channel = broadcast.subscribers[i].player.voiceConnection.channel;
						if (channel.id == message[2])
							channel.leave();
					}
					break;

			}
		}

	});

});

httpServer.on('upgrade', (req, socket, head) => {
	wsServer.handleUpgrade(req, socket, head, socket => {
		wsServer.emit('connection', socket, req);
	});
});


bot.login(cfg.discord_token);
